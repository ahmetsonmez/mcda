## Bütünleşik Ahp-Topsis-Vikor Uygulaması

### Özet

Çok kriterli karar verme yöntemleri; karar vericinin belirsizlik, karmaşıklık ve birbiriyle çelişen amaçların olduğu hallerde uygun seçenekler oluşturarak daha iyi karar vermesine yardımcı olmaktadır. Analitik Hiyerarşi Süreci (Analytic Hierarchy Process-AHP) ,Topsis (Technique for Order Preference by Similarity to Ideal Solution) ve Vikor (VlseKriterijumska Optimizacija I Kompromisno Resenje) oldukça fazla kullanılan çok kriterli karar verme yöntemlerindendirler. 

### Uygulama Hakkında
Bu uygulama, Sakarya Üniversitesi Fen Bilimleri Enstitüsü  Bilişim Sistemleri "Proje Ödevi" olarak geliştirilmiştir. Uygulama içeriğini oluşturan Ahp,Topsis ve Vikor ile iglili teknik dökümanlara ve uygulamaya http://mcdanew.azurewebsites.net/ adresi üzerinden erişebilirsiniz.

Yukarıda bahsedilen yapılar (Ahp-Topsis-Vikor) aklınıza gelebilecek tüm alanlarda kullanılmak ile beraber daha çok endüstri mühendisliği alanında kullanılmaktadır. Uygulamaların anlaşılması, hesaplamaların daha hızlı gerçekleştirilmesi ve aynı zamanda uygulamanın diğer yazılımcı arkadaşlar tarafından da geliştirilmesi  amacıyla Github'a konulmuştur. 
